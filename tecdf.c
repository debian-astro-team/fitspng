/*

  FITSPNG    -- test routine for empirical distribution function
  Copyright (C) 2019  Filip Hroch, Masaryk University, Brno, CZ

  Fitspng is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Fitspng is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Fitspng.  If not, see <http://www.gnu.org/licenses/>.

  $ gcc -Wall -g -p -no-pie ecdf.c tecdf.c -lm

*/


#include <stdio.h>
#include <stdlib.h>

int ecdf(long, const float *, float *, float *);
float quantile(long, const float *, const float *, float);

int main()
{
  float x[7] = {16.0, 12.0, 99.0, 95.0, 18.0, 87.0, 10.0 };
  long ncdf;
  float *xcdf = NULL, *ycdf = NULL;

  xcdf = malloc(7*sizeof(float));
  ycdf = malloc(7*sizeof(float));

  ncdf = ecdf(7,x,xcdf,ycdf);
  printf("%f (q(1/2) = 91)\n",quantile(ncdf,xcdf,ycdf,0.5));

  for(long i = 0; i < ncdf; i++)
    printf("%f %f\n",xcdf[i],ycdf[i]);

  free(xcdf);
  free(ycdf);
  return(0);
}
