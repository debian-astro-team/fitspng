<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="FITS to PNG format converter">
    <meta name="author" content="Filip Hroch">
    <meta name="keywords" content="FITS, PNG, astronomy, digital, photography, astrophotography">
    <title>Fitspng</title>
    <style>
  body { margin-left: auto; margin-right: auto; padding-top: 0em; padding-bottom: 0em; padding-left: 2em; padding-right: 2em; max-width: 50em;}
h1 { font-size: 140%; margin-left: 0%; margin-top: 1em; font-family: sans-serif; border-bottom: thin dotted #8AB8E6; color: #8AB8E6;}
h2 { font-size: 100%; font-family: sans-serif; margin-left: 0%; margin-top: 2em; border-bottom: thin dotted #8AB8E6; color: #8AB8E6;}
h3 { font-size: 100%; margin-left: auto; margin-right:0; max-width:45em;margin-top: 2em; color: #8AB8E6;}
pre { font-family: monospace; background: rgb(244,244,244); overflow: auto; max-width: 45em; margin-left: auto; margin-right: auto;}
p { max-width: 45em; margin-left: auto; margin-right: 0;}
p.indent:first-letter { padding-left: 2em; }
p.abstract { margin-top: 0em; margin-left: auto; margin-right: 0; margin-bottom:2em; text-align: right; font-style:italic;}
p.picture { font-size: 95%; text-align: center; margin-bottom: 2em; width: 352px;}
p.caption { font-size: 95%; text-align: center; margin-bottom: 2em;}
div.lp { float: left; }
div.rp { margin-left: 382px; }
div.pic { margin-left: auto; margin-right: 0; width: 750px;}
ul, dl { max-width: 45em; margin-left: auto; margin-right: 0;}
footer { font-size: 100%; margin-top: 5em; margin-bottom: 1em; padding-top: 1em; margin-left: auto; margin-right: 0; border-top: thin dotted; text-align: center; width: 100%}
ul, dl { max-width: 45em; margin-left: auto; margin-right: 0;}
li { margin-top: 0.5em; }
dt { margin-top: 1em; }
dd { margin-top: 0.2em; }
dl { margin-bottom: 1em; }
span.inline{ font-weight: bold; font-family: sans-serif; color: #8AB8E6; }
span.par{ font-weight: bold; font-family: sans-serif; color: #8AB8E6; margin-right: 0.618em; }
    </style>
  </head>

  <body>
    <header>
      <h1>Fitspng</h1>

      <p class="abstract">
	Fitspng is an utility intended to convert of images in astronomical
	FITS format into PNG format.
      </p>
    </header>

    <h2>Introduction</h2>

<p>
<a href="https://fits.gsfc.nasa.gov/">FITS</a> format is a general purpose
astronomical format which stores measurements of physical quantities.
FITS images represents an angular distribution of captured photon events,
or intensity of light.
Fitspng provides visualisation of
<a href="https://fits.gsfc.nasa.gov/">FITS</a>
images by converting them
to <a href="http://www.libpng.org/pub/png/">PNG</a> format.
</p>


<h2 id="abstone">Tone mapping</h2>

<p>
  Fitspng applies a
  <a href="https://en.wikipedia.org/wiki/Tone_mapping">global tone mapping</a>
  technique to transform of a wide dynamical range of FITS files
  (naturally including
  <a href="https://en.wikipedia.org/wiki/High_dynamic_range_imaging">HDR</a>)
  to a limited range of, so called, modern displaying devices and
  image formats.
</p>

<p class="indent">
  The tone mapping includes two steps:
  a pre-scaling (normalisation), and optional application of an intensity
  transfer function,
</p>

<p class="indent">
  <span class="inline">Pre-scaling</span>
  maps linearly the captured counts, related to optical
  <a href="https://en.wikipedia.org/wiki/Intensity_(physics)">intensities</a>
  <i>I</i> (<i>I ∈ ℝ, I ≥ 0 </i>), to the range
  <i>0 ≤ t ≤ 1</i> by the two parameter transformation:
</p>
<p class="indent">
<i>t = (I - B) / s</i>.
</p>
<p>
  As the parameters, <i>B</i> – black level, and
  <i>s</i> – sensitivity, has been selected.
</p>
<p class="indent">
  <i>B</i> sets a background level in original data. It corresponds to zero
  in transformed values, and finally to the black colour.
</p>
<p class="indent">
  The sensitivity <i>s > 0</i> adjusts
  a range of FITS values visible on PNG pictures.
  It simulates the artificial sensitivity of a detector:
  high, or low, values leads to a dim, or highlight, images.
  Sensitivity operates like a gain of an electronic amplifier of a detector.
  This parameter is a reciprocal quantity to ISO value well-known in
  classical photography.
</p>
<p class="indent">
  The pre-scale transformation
  should be followed by a cut-off, which assigns
  all values below the level <i>B</i> to be black,
  and over the interval to be white:
</p>
<p class="indent">
  <i>
    0 ≤ t ≤ 1
<!--    t = { (I - B) / s  ∈ (0, 1) }-->
  </i>
</p>
<p>
</p>

<p>
  <span class="inline">ITT scaling</span> applies an
  intensity transfer function <i>f(t)</i>
  (realised by an intensity transfer table (ITT) in the past)
  on the pre-scaled data <i>t</i>:
</p>
<p class="indent">
  <i>τ = f(t)</i>.
</p>

<p class="indent">
  Various ITT functions are
  introduced in <a href="#itt">ITT functions</a> paragraph.
</p>

<p class="indent">
  The described tone mapping is applied on all pixels of a frame;
  one is applied on values for grey-scale,
  and on the luminosity channel of colour images.
</p>

<p class="indent">
  Both <i>B, s</i> parameters can be leaved unspecified:
  they are estimated
  by the included machine algorithm described in
  <a href="#tone">Tone parameters estimation</a>.
  A manual setup of the pre-scale parameters is available in two modes:
</p>
<ul>
  <li><b>Absolute:</b>
    the parameters <i>B, s</i> are given directly by user or,
  <li><b>Relative:</b>
    their values are given as multipliers of
    internally estimated parameters.
  </li>
</ul>

<p class="indent">
  The absolute scaling is suitable for fine tune by hand, while
  the relative scaling for some batch processing.
</p>


<h2>Gamma correction</h2>

<p>
  Finally, a light-sensitivity model, specific to a displaying device,
<a href="https://en.wikipedia.org/wiki/SRGB">sRGB</a>
is applied onto <i>τ</i> values by the colour space specific
gamma function <i>Γ(τ)</i>:
</p>
    <p class="indent">
      <i>p = { 255 Γ(τ), 0 ≤ p ≤ 255 }</i>,
    </p>
<p>
where <i>p</i> is the final value stored in PNG.
</p>


<h2 id="tone">Tone parameters estimation</h2>

<p>
  A reliable initial setup of the scaling parameters <i>B, s</i> is crucial
  for proper function of Fitspng. A novel approach on base of quantiles of
  an empirical distribution function has been developed.
</p>

<p class="indent">
  <span class="inline">Background </span> level is estimated as
  25% <a href="https://en.wikipedia.org/wiki/Quantile">quantile</a>
  <i>Q</i><sub>B</sub> of
  <a href="https://en.wikipedia.org/wiki/Empirical_distribution_function">
    empirical CDF</a> of observed pixels.
  It is close to CDF of
  <a href="https://en.wikipedia.org/wiki/Normal_distribution">Normal
    distribution</a> for the sky, a star free background.
  The black level is claimed as the quantile:
</p>
<p>
  <i>
    B = Q<sub>B</sub>(¼)
  </i>
</p>
<p>
  The pixels included in determination of the CDF
  are selected from a grid covering full frame
  (over 30 thousands of data points). The one-quarter choice
  is a result of empirical experiments.
</p>

<p class="indent">
  <span class="inline">Light</span> upper level is estimated
  as 95% quantile of the empirical CDF constructed on base of
  pixels with values above <i>≥ 3 D</i>
  of the background CDF. The selected pixels contains high fraction
  of star light and another star-gazing objects,
  which provides the range for observed intensity values.
</p>
<p>
  <i>
    s<sub>0</sub> = Q<sub>L</sub>(95%)
  </i>
</p>
<p>
  Parameter <i>D</i> is a quantile estimation of dispersion
  on base of ¼ and ¾ of <i>Q</i><sub>B</sub>
  quantiles (<i>Q<sub>N</sub>(½) ≐ 0.6745</i> is 50% quantile of
  Normal distribution):
</p>
<p>
  <i>
    D = [Q<sub>B</sub>(¾) - Q<sub>B</sub>(¼)] / [2 · Q<sub>N</sub>(½)]
  </i>
</p>

<p class="indent">
  <span class="inline">Relative parameters <i>q, v</i></span>
  are defined by the way:
</p>
<ul>
  <li>
    <i>B = Q<sub>B</sub>(q)</i>
  </li>
  <li>
    <i>s = v s<sub>0</sub></i>
  </li>
</ul>

<p class="indent">
  The relative parameter <i>q ∈ (0,1)</i> is the quantile of
  <i>Q<sub>B</sub>(q)</i>. It is set on ¼ for grey images.
  Colour images has both <i>B</i> and <i>Q<sub>B</sub>(q)</i>
  set such way to have guarantied <i>B &ge; 0</i>;
  <a href="https://en.wikipedia.org/wiki/CIE_1931_color_space">CIE 1931 XYZ</a>
  values are positive by definition.
</p>

<p class="indent">
  Common properties of <i>Q<sub>B</sub>(q)</i> are:
  <i>q = ½</i> is arithmetic mean,
  <i>q = ¼</i> (standard background level) is the mean of absolute deviations
  under the mean, <i>q=0</i> or <i>q=1</i> are minimal and maximal values.
</p>

<p class="indent">
  The parameter <i>0.001 &lt; v &lt; 1000</i> (mostly)
  adjusts relative slope against to the pre-defined value
  <i>s<sub>0</sub></i>.
</p>

<!--
gnuplot << EOF
set term svg size 640,320 dynamic linewidth 2
set output 'bcdf.svg'
set xrange[400:1500]
set key right bottom
plot 'b.dat' w l t "Image data", 0.5*(1+erf((x-706.88)/1.41/65.88)) t "Normal distribution"
set output 'bhist.svg'
set yrange[1:30500]
set xrange[1:3000]
set logscale y
plot 'bh.dat' w histeps t "Image data histogram", 26e3*exp(-(x-706.88)**2/2/65.88**2) w l t "Normal distribution density", 0.6e2*(x/2000)**-1.5 t "Asymptotic approximation"
EOF
-->

<p class="indent">
  The empirical CDF for a perfectly ordinary picture of the sky,
  a starry image having its principal part covered by a noise due background,
  is plotted on the graph below.
  The vertical axis shows quantiles, the horizontal axis of picture values.
  The background noise follows Normal distribution up to <i>Q</i> = 0.8;
  the star light is getting importance over the background above the level.
</p>

<div class="pic">
<img src="images/bcdf.svg" alt="cdf">
<p class="caption">
  The empirical CDF of a perfectly ordinary picture of the sky,
  see text for description.
</p>
</div>

<div class="pic">
<img src="images/bhist.svg" alt="histogram">
<p class="caption">
  The histogram of the same frame.
</p>
</div>

<h2 id="itt">ITT functions</h2>

<p>
  Intensity transfer (ITT) functions maps input and output
  intensities with non-linear weights. It can help to emphasise,
  or to surprise, desired objects on images. The choice of "right"
  function depends on taken scene, on photographers intention,
  and it is so unique as every photography is unique.
</p>

<p>
  Fitspng implements these ITT functions:
</p>

<ul>
  <li>line: <i>f(t) = t</i>,</li>
  <li>S/N like: <i>f(t)</i> = <i>at</i> / asinh(<i>at</i>) - 1,
    <i>t &gt; 0, t ≈ 6</i>,</li>
  <li>square (sqr): <i>f(t) = t<sup>2</sup></i>,</li>
  <li>tanh: <i>f(t)</i> = tanh(<i>t</i>),</li>
  <li>asinh: <i>f(t)</i> = asinh(<i>t</i>),</li>
  <li>photo: <i>f(t)</i> = ½ {1 + tanh[2(<i>t</i>-1)]}.</li>
</ul>

<p class="indent">
  <span class="inline">Line</span> function does nothing,
  so it is very universal, fast and the preferable first choice.
</p>

<p class="indent">
  <span class="inline">S/N like</span> function is very rough approximation
  of signal-to-noise ratio which offers a good image contrast.
  For pure counts, the ratio is proportional
  to reciprocal of relative error of observed values as <i>t / √t</i>,
  but the square root has singularity in zero. Asinh is a safe approximation
  of square root for <i>0 ≤ t ≤ 10</i>.
</p>

<p class="indent">
  Function <span class="inline">square</span> emphasises details in
  large diffuse faint areas.
</p>

<p class="indent">
  Function <span class="inline">tanh</span> simulates
  <a href="https://en.wikipedia.org/wiki/Logistic_function">logistics curve</a>
  with a smooth saturation.
</p>

<p class="indent">
  Function <span class="inline">asinh</span> offers the most wide dynamical range.
  It keeps faint details white the bright objects are still visible.
</p>

<p class="indent">
  <span class="inline">Photo</span> emulates the gradation curve of
  the classical photographic emulsion having a low dynamical range.
</p>

<!--
gnuplot << EOF
set term svg size 640,320 dynamic linewidth 2
set output 'itt.svg'
set xrange[0:2.3]
set yrange[0:1]
set key right bottom
plot x t "line", 6.66*x/asinh(6.66*x)-1 t "S/N like", x**2 t "square",\
     tanh(x) t "tanh", asinh(x) t "asinh", (1 + tanh(2*(x - 1)))/2 t "photo"
EOF
-->

<div class="pic">
<img src="images/itt.svg" alt="Itt">
</div>


<h2 id="colour">Colour tuning</h2>

<p>
  Colour FITS frames, as defined by
  <a href="https://munipack.physics.muni.cz/colorfits.html">Munipack</a>,
  are recognised.
</p>

<p class="indent">
  <span class="par">Saturation</span>
  The colour saturation of images should be reduced when
  the relative saturation parameter has value <i>S &lt; 1</i>,
  or enhanced when <i>S &gt; 1</i>. The saturation of the final frame
  is computed by formula
</p>
<p>
  <i>S c</i>,
</p>
<p>
  with the chroma <i>c = √[(u-u</i><sub>w</sub><i>)<sup>2</sup> +
    (v-v</i><sub>w</sub><i>)<sup>2</sup>]</i> specifying the white-point distance in
  <a href="https://en.wikipedia.org/wiki/CIELUV">CIELUV colour-space</a>.
</p>

<p class="indent">
  <span class="par">Nite vision</span>
  The human eye sensitivity in high and low light conditions
  can be artificially simulated by greying of faint parts of images.
  The night vision has shifted spectral sensitivity into short wavelengths
  which, together with its narrow broadband, simulates
  <a href="https://en.wikipedia.org/wiki/Purkinje_effect">Purkyně effect</a>.
</p>

<p>
  The blend of photopic (daily) and scotopic visions (darkness)
  is modelled by the weighting function
</p>
<p>
  <i>w =  ½ {1 + </i>tanh<i> [(Y - Y<sub>m</sub>) / w<sub>m</sub>]}</i>,
</p>
<p>
  with night-day vision threshold <i>Y<sub>m</sub></i>,
  and width of <i>w<sub>m</sub></i> of the mesopic vision region.
  The function has been selected by heuristic. There is no a proof of validity.
</p>
<p class="indent">
  To derive composition of colour and scotopic images,
  <a href="https://en.wikipedia.org/wiki/CIE_1931_color_space">CIE 1931 XYZ</a>
  values are converted into monochromatic scotopic channel <i>S</i>
</p>
<p>
  <i>S = 0.3617 Z + 1.1821 Y - 0.8050 X.</i>
</p>
<p>
  Mesopic intensity <i>Y</i><sub>m</sub>
  is a compound of intensities <i>Y</i> and <i>S</i>:
</p>
<p>
  <i>Y</i><sub>n</sub> =
  <i>wY</i> + <i>(1-w)S</i> = <i>Y</i><sub>w</sub> + <i>S</i><sub>w</sub>,
  &nbsp; &nbsp; &nbsp;
with <i>Y</i><sub>w</sub> = <i>Yw</i>, <i>S</i><sub>w</sub> = <i>S(1-w)</i>.
</p>
<p>
  Colours are derived by chromaticity coordinates xy in xyY colour space
  (<a href="https://en.wikipedia.org/wiki/CIE_1931_color_space">CIE 1931 XYZ</a>,
  sec. CIE xy chromaticity diagram and the CIE xyY colour space)
  by a blend of full colours with scotopic channel. Scotopic vision is
  in degrees of grey having chromatic coordinates of the white-point
  <i>x</i><sub>w</sub>,<i>y</i><sub>w</sub>:
</p>
<p>
  <i>x</i><sub>n</sub> =
    [(<i>x/y</i>) <i>Y</i><sub>w</sub> +
            (<i>x</i><sub>w</sub>/<i>y</i><sub>w</sub>) <i>S</i><sub>w</sub>]
            / <i>D</i>
</p>
<p>
  <i>y</i><sub>n</sub> = <i>Y</i><sub>n</sub> / <i>D</i>,
</p>
<p>
  where the denominator is <i>D</i> =
  <i>Y</i><sub>w</sub> / <i>y</i> + <i>S</i><sub>w</sub> / <i>y<sub>w</sub></i>.
</p>

<p class="indent">
  The scotopic imaging is possible under conditions
  <i>Y<sub>m</sub> &ge; 0</i> and
  <i>w<sub>m</sub> &gt; 0</i>. Pictures becomes fully coloured
  for <i>Y<sub>m</sub> → 0</i>, or complete in night vision for
  <i>Y<sub>m</sub> → ∞</i>. The mesopic regime
  parameters <i>Y<sub>m</sub></i>, <i>w<sub>m</sub></i>
  should be determined empirically by a qualified estimate.
</p>


<h2>Image resize</h2>
<p>
An output image can be scaled-down by an integer factor. Every pixel
of the scaled image is computed as the arithmetical mean of a square
with side given by this factor.
The photometric information is preserved while a noise is suppressed
during the re-scaling. This sufficient fast method provides high quality images.
</p>

<h2>Exif information</h2>

<p>
  FITS header meta-information is stored as an EXIF information
  of PNG files: the EXIF text strings has free format and no formalised
  form.
</p>


<h2>Fitspng invocation</h2>

<h3>Synopsis</h3>

<p>
  <b>fitspng</b> [options] file(s)
</p>

<h3>Command line options</h3>

<dl>

<dt><b>-r</b> <i>q, v</i></dt>
<dd>
  <p>
  Relative pre-scaling mode: <i>q, v</i> (<i>0 ≤ q ≤ 1</i>)
  represents quantiles
  of a background (the sky) and <i>v</i> relative sensitivity.
  See <a href="#tone">Tone parameters estimation</a>.
  </p>
  <p>
    Parameters can be omitted leaving default values in row.
    It is specially useful for colour images where <i>q</i>
    should have its value slightly over zero. The exact text form is
    <samp>"-r ,v"</samp> or <samp>"-r q,"</samp>, and should have
    the comma included to support unambiguous interpretation.
  </p>
  <p>
    If a colour image is processed, <i>q</i> is determined as the quantile
    for zero intensity, so it is good idea to leave it unset; just use
    <samp>"-r ,v"</samp>.
  </p>
</dd>

<dt><b>-l</b> <i>B, s</i></dt>
<dd>
  Absolute pre-scaling mode: <i>B, s</i> are directly used
  for scaling of the output frame.
  This setup completely disables the internal parameters estimation.
  See <a href="#abstone">Tone mapping</a>.
</dd>

<dt><b>-f</b> [line | asinh | snlike | sqr | tanh | photo]</dt>
<dd>
  Select a function, see <a href="#itt">ITT functions</a> section.
</dd>

<dt><b>-S</b> <i>S</i></dt>
<dd>
The colour saturation (Colour FITS only).
</dd>

<dt><b>-n</b> <i>Y<sub>m</sub>, w<sub>m</sub></i></dt>
<dd>
  When used, switch-on, and <a href="#colour">setup</a>,
  mode which emulates human night vision (Colour FITS only).
</dd>

<dt><b>-s</b> <i>s</i></dt>
<dd>
  Shrink image: scale-down the size of the image by the specified factor
  <i>s &gt; 1</i>: every output pixel is the arithmetic mean of
  <i>s<sup>2</sup></i> input pixels.
</dd>

<dt><b>-o</b> filename</dt>
<dd>
  <p>
    Specify an output file name for a single file input only.
  </p>
  <p>
    If this switch is omitted, the output filename is determined by
    modification of the input filename(s): suffixes, like <samp>*.fits</samp>,
    are replaced by <samp>*.png</samp>, and the directory
    path is removed: <samp>/space/image.fits</samp> is converted
    to <samp>image.png</samp>. The approach leaves original
    data untouched, results are stored in the current working
    directory (see also <a href="#examples">Examples</a>).
    </p>
</dd>

<dt><b>-B</b> [8|16]</dt>
<dd>
  <p>
    The bit depth of the output PNG frame: 8 (default) or 16 bites per pixel.
  </p>
  <p>
    A post-processing can be affected by the choice; most utilities
    doesn't work with 16-bit colour depth correctly.
  </p>
</dd>

<dt><b>-v</b>, --verbose</dt>
<dd>
describes what Fitspng currently does.
</dd>

<dt><b>-h</b>, --help, --version</dt>
<dd>
prints summary of options; the current version.
</dd>
</dl>


<h2 id="examples">Examples of usage</h2>

<p>
Convert a FITS image to PNG:
</p>
<pre>
$ fitspng grey.fits
</pre>

<p>
Convert a FITS image to PNG thumbnail:
</p>
<pre>
$ fitspng -s 10 grey.fits
</pre>

<p>
Emulate the classical photography sensitivity function (gradation curve):
</p>
<pre>
$ fitspng -f photo photo.fits
</pre>

<p>
Create a semi-grey image:
</p>
<pre>
$ fitspng -S 0.2 -o reduced.png colour.fits
</pre>

<p>
Emulate night vision:
</p>
<pre>
$ fitspng -n 1e6,1 -o scotopic.png colour.fits
</pre>

<p>
  Generate thumbnails of FITS files in <samp>/space</samp>
  storing files under the current directory:
</p>
<pre>
  fitspng -s 10 /space/*.fits
</pre>
<p>
  The same result should be emulated with help of shell scripting
  (providing powerful file name manipulations):
</p>
<pre>
  $ for FITS in /space/*.fits; do
      NAME=$(basename $FITS);
      PNG=${NAME%fits}png;
      fitspng -o $PNG $FITS;
  done
</pre>

<p>
  Fitspng has no parallel run support included;
  however, the execution time on multiprocessor systems should be significantly
  reduced with help of some external utility:
</p>
<pre>
  $ ls /space/*.fits | xargs -n 1 -P 0 fitspng
</pre>
<p>

<h2>Gallery</h2>


<div style="padding:1em;"></div>

<div class="pic">
<div class="lp">
<img src="images/IMG_5952.png" width="352" height="234" alt="IMG_5952">
<p class="picture">
#1, Colour image in sRGB
</p>
</div>

<div class="rp">
<img src="images/IMG_5952_photo.png" width="352" height="234" alt="IMG_5952">
<p class="picture">
#2, ITT photography tone applied
</p>
</div>
</div>

<div class="pic">
<div class="lp">
<img src="images/IMG_5952_r02.png" width="352" height="234" alt="IMG_5952">
<p class="picture">
#3, Sensitivity increased
</p>
</div>

<div class="rp">
<img src="images/IMG_5952_r05.png" width="352" height="234" alt="IMG_5952">
<p class="picture">
#4, Sensitivity decreased
</p>
</div>
</div>

<div class="pic">
<div class="lp">
<img src="images/IMG_5952_S05.png" width="352" height="234" alt="IMG_5952">
<p class="picture">
#5, Reduced colour saturation
</p>
</div>

<div class="rp">
<img src="images/IMG_5952_S15.png" width="352" height="234" alt="IMG_5952">
<p class="picture">
#6, Enhanced colour saturation
</p>
</div>
</div>

<div class="pic">
<div class="lp">
<img src="images/IMG_5952_S0.png" width="352" height="234" alt="IMG_5952">
<p class="picture">
#7, Vanished colour saturation
</p>
</div>

<div class="rp">
<img src="images/IMG_5952_nite.png" width="352" height="234" alt="IMG_5952">
<p class="picture">
#8, Night vision
</p>
</div>
</div>


<p>
  Whole gallery is generated by processing of the reference raw photo
<a href="https://integral.physics.muni.cz/rawtran/IMG_5952.CR2">IMG_5952.CR2</a>:
</p>
<pre>
$ rawtran IMG_5952.CR2
$ fitspng                   -s 10 -o IMG_5952.png       IMG_5952.fits #1
$ fitspng -f photo -l 0,4e3 -s 10 -o IMG_5952_photo.png IMG_5952.fits #2
$ fitspng -r 0,2            -s 10 -o IMG_5952_r02.png   IMG_5952.fits #3
$ fitspng -r 0,0.5          -s 10 -o IMG_5952_r05.png   IMG_5952.fits #4
$ fitspng -S 0.5            -s 10 -o IMG_5952_S05.png   IMG_5952.fits #5
$ fitspng -S 1.5            -s 10 -o IMG_5952_S15.png   IMG_5952.fits #6
$ fitspng -S 0              -s 10 -o IMG_5952_S0.png    IMG_5952.fits #7
$ fitspng -n 3e10,1         -s 10 -o IMG_5952_nite.png  IMG_5952.fits #8
</pre>

<h2>Download and installation</h2>

<p>
  The <a href="https://integral.physics.muni.cz/ftp/fitspng/">tar-ball</a>,
  or the <a href="https://integral.physics.muni.cz/hg/fitspng/">development
    repository</a>, is freely available under GPL-3 licence.
  Both
  <a href="https://heasarc.gsfc.nasa.gov/fitsio/">cfitsio</a> and
  <a href="http://www.libpng.org/pub/png/libpng.html">libpng</a>
  libraries, including files required for development (headers, static libraries),
  are necessary for building.
</p>

<ul>
<li>
Fitspng runs under any Unix-like operating system (all flavours
of GNU/Linux and BSD, Solaris, Mac OS X, etc).
<li>
  Fitspng packages can be found in
  <a href="https://packages.debian.org/fitspng">Debian</a> and
  <a href="https://packages.ubuntu.com/search?keywords=fitspng">Ubuntu</a>.
</li>
<li>
Jean-Paul Godard reported successful compilation under W7 (64b) environment
and mingw.
</li>
<li>
Fitspng can be compiled under Windows 8 (64-bit) with mingw
compiler (libpng will be required).
</li>
</ul>

<p><a href="https://www.gnu.org/software/automake/manual/html_node/Basic-Installation.html">
A recommended way</a> of installation under Unix-like system is:</p>

<pre>
$ tar zxf fitspng-U.V.tar.gz
$ cd fitspng/
$ ./configure CFLAGS="-O2 -DNDEBUG"
$ make
# make install
</pre>

<p>The final step should be executed by root.
  All files are installed into <code>/usr/local</code> tree.
  It would be nice to keep the source directory, if uninstall is
  a desired action.
</p>


<h2>See also</h2>
<p>
<a href="https://munipack.physics.muni.cz">Munipack</a> is a general utility
working with FITS images.
</p>

<p>
Development notes can be found in
<a href="https://integral.physics.muni.cz/pelican/tag/fitspng.html">my blog.</a>
</p>

<h2>License</h2>
<p>
  Fitspng is <a href="https://www.gnu.org/philosophy/free-sw.html">free
    software</a> licensed under the
  <a href="https://www.gnu.org/licenses/gpl.html">GNU General Public License</a>.
  This gives you the freedom to use and modify Fitspng to suit your needs.
</p>

  <footer>
    Copyright &copy; 2006 — 2022, <a href="mailto:hroch@physics.muni.cz">F. Hroch</a>,
    <a href="https://www.physics.muni.cz">Institute of Theoretical Physics and
      Astrophysics</a>,
    <a href="https://www.muni.cz/">Masaryk University</a>,
    <a href="https://en.wikipedia.org/wiki/Brno">Brno</a>,
    <a href="https://en.wikipedia.org/wiki/Czech_Republic">Czech Republic</a>.
  </footer>

</body>
</html>
