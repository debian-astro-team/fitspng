/*

  FITSPNG     FITS to PNG converter
  Copyright (C) 2006-2022  Filip Hroch, Masaryk University, Brno, CZ

  Fitspng is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Fitspng is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Fitspng.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "fitspng.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <float.h>

#include <assert.h>

#if defined _WIN32 || defined __CYGWIN__
#define DIRSEP '\\'
#else
#define DIRSEP '/'
#endif

void help();
void version();
char *pngsuff(const char *);

int main(int argc,char *argv[])
{
  char *png = NULL;
  int bit_depth, scale, type, verb;
  float qblack,rsense,t,s,st,ss, satur;
  int i, set_abs, set_rel, set_fn;
  int nfits = 0;
  int status = 0;

  bit_depth = 8;
  scale = 1;
  type = 0;
  verb = 0;
  set_abs = 0; set_rel = 0; set_fn = 0;

  qblack = 0.25;
  rsense = 1.0;
  t = 0.0;
  s = 1.0;
  satur = 1.0;

  if( argc == 1 ) {
    help();
    return(0);
  }

  for( i = 1; i < argc; i++) {

    /* help */
    if( strcmp(argv[i],"--help") == 0 || strcmp(argv[i],"-h") == 0 ){
      help();
      return(0);
    }

    /* version */
    if( strcmp(argv[i],"--version") == 0 ){
      version();
      return(0);
    }

    /* verbose */
    else if( strcmp(argv[i],"-v") == 0 || strcmp(argv[i],"--verbose") ==0 ){
      verb = 1;
    }

    /* output name */
    else if( strcmp(argv[i],"-o") == 0 && i++ < argc ){
      png = argv[i];
    }

    /* bit depth */
    else if( strcmp(argv[i],"-B") == 0 && i++ < argc ){

      if( !( sscanf(argv[i],"%d",&bit_depth) == 1 &&
	     (bit_depth == 8 ||  bit_depth == 16)) ) {
        fprintf(stderr,"Error: you should set bitpix -B to value 8 or 16.\n");
        return(1);
      }
    }

    else if( strcmp(argv[i],"-f") == 0 && i++ < argc ){

      /* line */
      if( strcmp(argv[i],"line") == 0 )
	type = 0;

      /* atan */
      else if( strcmp(argv[i],"asinh") == 0 )
	type = 1;

      /* square root */
      else if( strcmp(argv[i],"snlike") == 0 )
	type = 5;

      /* square */
      else if( strcmp(argv[i],"sqr") == 0 )
	type = 6;

      /* tanh */
      else if( strcmp(argv[i],"tanh") == 0 )
	type = 7;

      /* photo */
      else if( strcmp(argv[i],"photo") == 0 )
	type = 8;

      else {
        fprintf(stderr,"Error: an unknown identifier for tone function.\n");
        return(1);
      }
    }

    /* linear scale */
    else if( (strcmp(argv[i],"-l")==0 || strcmp(argv[i],"-fl")==0) && i++ < argc ){
      if( sscanf(argv[i],"%f,%f",&t,&s) == 2 && s > 0.0 )
	set_abs = 1;
      else {
	fprintf(stderr,"Error: absolute scale(-l): needs pair of reals; "
		"comma separated, second > 0.\n");
	return(1);
      }
    }

    /* relative intensity scale */
    else if( (strcmp(argv[i],"-r")==0 || strcmp(argv[i],"-fr")==0) && i++ < argc ){
      if( sscanf(argv[i],"%f,%f",&qblack,&rsense) == 2 && rsense > 0.0 )
	set_rel = 1;
      else if( sscanf(argv[i],",%f",&rsense) == 1 && rsense > 0.0 )
	set_rel = 3;
      else if( sscanf(argv[i],"%f,",&qblack) == 1 )
	set_rel = 2;
      else {
        fprintf(stderr,"Error: relative scale(-r): needs pair of reals;"
		" comma separated (first in 0..1, second>0).\n");
        return(1);
      }
    }

    /* colour saturation factor */
    else if( (strcmp(argv[i],"-S")==0 || strcmp(argv[i],"-fs") == 0) && i++ < argc ){
      if( !(sscanf(argv[i],"%f",&satur) == 1 && satur >= 0.0) ) {
        fprintf(stderr,"Error: saturation(-S): requires a positive (or zero) real.\n");
        return(1);
      }
    }

    /* scotopic threshold */
    else if( (strcmp(argv[i],"-n")==0 || strcmp(argv[i],"-fn")==0) && i++ < argc ){
      if( sscanf(argv[i],"%f,%f",&st,&ss) == 2 && ss > 0.0)
	set_fn = 1;
      else {
        fprintf(stderr,"Error: scotopic(-n) needs pair of reals; "
		"comma separated, second > 0.\n");
        return(1);
      }
    }

    /* shrink */
    else if( strcmp(argv[i],"-s") == 0 && i++ < argc ){
      if( !(sscanf(argv[i],"%d",&scale) == 1 && scale > 0 ) ) {
        fprintf(stderr,"Error: shrink option(-s) requires a positive decimal.\n");
        return(1);
      }
    }

    else {

      if( *argv[i] == '-' )
	fprintf(stderr,"Warning: an unknown parameter? (%s).\n",argv[i]);

      /* keep the index of the first FITS file on command line  */
      if( nfits == 0 )
	nfits = i;
    }

  }

  if( nfits == 0 ) {
    fprintf(stderr,"Error: unspecified FITS file(s).\n");
    return 1;
  }

  if( argc - nfits > 1 && png ) {
    fprintf(stderr,"Error: Set of output filename (by -o) is uncompatible"
	  " with list of FITSes.\n");
    return 1;
  }

  if( verb )
    fprintf(stderr,"Default intensity scaling: qblack=%f rsense=%f. Itt type=%d.\n",
	    qblack,rsense,type);

  status = 0;
  for( i = nfits; i < argc; i++ ) {
    char *pngname = png ? strdup(png) : pngsuff(argv[i]);
    int code = fitspng(argv[i],pngname,bit_depth,qblack,rsense,scale,t,s,st,
		       ss,type,satur,verb,set_abs,set_rel,set_fn);
    if( code != 0 ) {
      status = 2;
      fprintf(stderr,"Error: fitspng failed for `%s'.\n",argv[i]);
    }

    free(pngname);
  }

  return status;
}


void help()
{
  printf("%s\n\n%s\n\n%s\n%s\n %s\n%s\n %s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n",
	 "FITSPNG    FITS to PNG converter",
	 "Usage: fitspng [options] file(s)",
	 "options:",
	 "\t-r q,v\t relative intensity scale parameters: 0<=q<=1, v>0",
	 "\t-l B,s\t absolute intensity scale parameters: (x-B)/s",
	 "\t-f str\t tone function: line (default), asinh, snlike, sqr, tanh, photo",
	 "\t-S x\t colour saturation (default: 1)",
	 "\t-n Y,w level and width for night/day vision (try: 1e6,1)",
	 "\t-s s\t zoom-out image by factor 1/s to create thumbnails (default: 1)",
	 "\t-o str\t output filename (considered for a single file input)",
	 "\t-B [8|16]\t bits per pixel of output image, default: 8",
	 "\t-v\t\t verbose mode",
	 "\t--help\t\t give this help",
	 "\t--version\t display software version");
}


void version()
{
  fprintf(stdout,"FITSPNG, %s, (C) 2006-2022 F. Hroch,"
	  " Masaryk University in Brno, CZ\n",VERSION);
  fprintf(stdout,"FITSPNG comes with ABSOLUTELY NO WARRANTY.\n");
  fprintf(stdout,"You may redistribute copies of FITSPNG\n");
  fprintf(stdout,"under the terms of the GNU General Public License.\n");
  fprintf(stdout,"For more information about these matters, see the file named COPYING.\n");
}

char *pngsuff(const char *fitspath)
{
  char *png = 0;
  const char *fits = 0, *dot = 0;

  /* extracting basename */
  if( (fits = strrchr(fitspath,DIRSEP)) == NULL )
    fits = fitspath;
  else
    fits++;

  /* suffix like ".fits" is replaced by ".png" */
  if( (png = malloc(strlen(fits) + 5)) == NULL )
    perror(__FILE__);
  *png = '\0'; /* should be initialised with null-terminating byte */
  assert(png && fits);
  if( (dot = strchr(fits,'.')) != NULL )
    strncat(png,fits,dot-fits);
  else
    strcat(png,fits);
  strcat(png,".png");

  return png;
}
