/*

  FITSPNG

  Copyright (C) 2006-2022  Filip Hroch, Masaryk University, Brno, CZ

  Fitspng is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Fitspng is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Fitspng.  If not, see <http://www.gnu.org/licenses/>.

*/



/* fitspng.c */
int fitspng(char *, char *,int,float,float,int,
	    float,float,float,float,int,float,
	    int,int,int,int);

/* ecdf.c */
int ecdf(long, const float *, float *, float *);
float quantile(long, const float *, const float *, float);

/* tone.c */
int tone(long, const float *, float, float*,float *, float *,int,int,int);
