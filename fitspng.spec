Summary: FITS to PNG converter
Name: fitspng
Version: %(cat configure.ac | awk '{if(/AC_INIT/) {gsub("[\\[\\]\\,]"," ");  print $3;}}')
Release: 1
License: GPL3
Group: Applications/File
URL: http://integral.physics.muni.cz/%{name}
Source0: ftp://integral.physics.muni.cz/pub/%{name}/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description
Fitspng is an utility intended to convert of the natural high
dynamic range of FITS images, directly representing measured data,
to the limited numerical range of PNG format widely used
in computer graphics. Fitspng implements a global tone mapping
technique by a set of tone functions using parameters provided
by user or by machine estimate on base of a robust count statistics.
Moreover, the conversion keeps an important FITS meta-information
as a text part of PNG header.

%prep
%setup -q

%build
%configure CPPFLAGS=-I/usr/include/cfitsio
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc %{_mandir}/man1/fitspng.1*
%{_bindir}/fitspng
%doc README COPYING NEWS
